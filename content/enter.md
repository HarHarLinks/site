---
title: "How to get to HSBXL?"
linktitle: "How to get to HSBXL?"
---

**Address:** Rue Osseghem 53, 1080 Molenbeek, Brussels, Belgium.
([open streetmap](https://www.openstreetmap.org/search?whereami=1&query=50.85516%2C4.32111#map=19/50.85516/4.32111))
([GoogleMaps](https://www.google.com/maps/place/Hackerspace+Brussels+HSBXL/@50.8552895,4.3188495,17z/data=!3m1!4b1!4m6!3m5!1s0x47c3c37ae6875a27:0x962d7fc36e046fa7!8m2!3d50.8552861!4d4.3214244!16s%2Fg%2F12lk5rsf3?entry=ttu))

# We are now located in LionCity

HSBXL has moved to LionCity, a vibrant and innovative space in the heart of Brussels. Situated in the former Delhaize distribution centre in Molenbeek, LionCity is home to a diverse mix of crafts, entrepreneurs, social projects, culture, sports, and urban agriculture.

For more details on LionCity, visit [here](https://www.entrakt.be/en/lioncity).

# How to get to the address

{{< image src="/images/enter_space/hsbxl_lioncity.png" >}}

© [OpenStreetMap contributors](https://www.openstreetmap.org/copyright) ♥

### For assistance getting in

- **Chat**: Join us at [Matrix chat](https://matrix.to/#/#hsbxl:matrix.org). Preferred for writing ahead of time.
- **Phone**: [+32 2 880 40 04](tel:+3228804004). Available when members are present in the space.

## By public transport 🚆

the hackerspace is just a few dozen meter from **metro Beekkant**
Most metrolines in brussels (1,5,2,6) stop at this station

If you are transfering from train to metro it is easiest to do so at the Central station (line 1 to gare de l'Ouest or 5 to Erasme) or south station (line 2 to simonis leopold II or 6 to roi baudoin)
The nearest train station is west station.

**Entrance Instructions**
Upon reaching LionCity, if the gate is cosed call the phone number above a member will guide you from there.
(Detailed entrance instructions will be provided in a few weeks.)

## By car 🚘

Parking is possible in the streets, or in the parking building nearby. (Detailed parking instructions will be provided in a few weeks.)

## Shared mobility

Brussels has a [vast offering in shared mobility](https://www.brussels.be/shared-mobility). Use a bike, step, or even car using the appropriate plans.

Keep in mind that Brussels has enforced drop zones for shared mobility bikes, electric scooters, and kick scooters (aka trottinette or step). Please park responsibly!

## At night: Collecto

To get back to your home/hotel, there is [Collecto](https://en.collecto.be/). A Taxi picks you up at certain points (many STIB public transport stops) and brings you to your destination within the Brussels region for the fixed price of 6 euro.

You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you have a smartphone, you can use the [Android](https://play.google.com/store/apps/details?id=be.tradecom.collecto&hl=en&gl=US) or [Apple iOS](https://apps.apple.com/be/app/collecto-your-shared-taxi/id921558166) app, otherwise, you can call +32 2 800 36 36 to reserve your Collecto.

There is a [collecto](https://en.taxisverts.be/collecto) startpoint at the entrance of Beekant STIB metro station, at 100m away.

# How to get to the Hackerspace once you have reached LionCity


From the gate at the corner **nr 53** walk into the building, in front of you you see a roofed street between 2 buidling. enter this roofed street, the entrance to the hackerspace is the first door on the right. If needed there is a door-bel on the right hand of the door 
If the gate is closed **please call +32 2 880 40 04** so someone can come help you get in. 

{{< image src="/images/enter_space/front-of-building.png" >}}

---
