---
title: "HSBXL Public Books"
linktitle: "HSBXL Books"
---

    Stats generated automatically from our bookkeeping.
    Code at https://gitlab.com/hsbxl/bookkeeper

<div class="accordion">

<h2 class="header">˅ 2019 (till {{< latest_statement >}})</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2019">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2019" >}}
</div>
<h2 class="header">˅ 2018 </h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2018">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2018" >}}
</div>
<h2 class="header">˅ 2017 </h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2017">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2017" >}}
</div>
<h2 class="header">˅ 2016</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2016">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2016" >}}
</div>
<h2 class="header">˅ 2015</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2015">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2015" >}}
</div>
<h2 class="header">˅ 2014</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2014">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2014" >}}
</div>
<h2 class="header">˅ 2013</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2013">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2013" >}}
</div>
<h2 class="header">˅ 2012</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2012">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2012" >}}
</div>
<h2 class="header">˅ 2011</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2011">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2011" >}}
</div>
<h2 class="header">˅ 2010</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2010">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2010" >}}
</div>
<h2 class="header">˅ 2009</h2>
<div>
<h3>Incoming</h3>
{{< books_incoming year="2009">}}
<h3>Outgoing</h3>
{{< books_outgoing year="2009" >}}

</div></div>

# Bank account

{{< bankaccount_graph year="2019" >}}

The [HSBXL bank account](/contact) contained on **{{< warchest date >}}** the amount of **€{{< warchest amount >}}**.
