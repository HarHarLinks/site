---
title: "Network/Ldap"
linktitle: "Network/Ldap"
date: 2018-11-06T22:08:31+01:00
state: archived
maintainer: "askarel"
---

# Getting started

    root@xm1:~# apt-get install slapd ldap-utils migrationtools ldapscripts

# Replication

# Adding hosts

When you want to give shell access to a Linux desktop/server you install
to HSBXL userbase, there are a few needed steps:

- Request a machine account in the ou=machines,dc=hsbxl,dc=be
  organizational unit
- apt-get install libpam-ldapd
  - Give the IP address of the LDAP server (currently 192.168.255.1)
  - Give the base DN (dc=hsbxl,dc=be)
  - Say yes to all options
- Edit /etc/pam.d/common-account and add the following line at the
  end:

`session    required   pam_mkhomedir.so skel=/etc/skel/ umask=0022`

- Edit the /etc/nslcd.conf file:
  - Verify that the server URI is correct:

`uri `<ldap://192.168.255.1>

- - Add the machine account details

`binddn uid=`<machinename>`,ou=machines,dc=hsbxl,dc=be`  
`bindpw `<your machine account password>  
`ignorecase yes`

- Stop the nscd cache daemon (this is the cache daemon, and can get in
  the way during the testing phase)

`/etc/init.d/nscd stop`

- Restart the nslcd daemon

`/etc/init.d/nslcd restart`

- Use this command to see if you get more users than what's defined in
  /etc/passwd:

`getent passwd`

- If you get the user list from the LDAP server, your setup is working
  and you can restart the nscd daemon:

`/etc/init.d/nscd start`

# SUDO

Yes, sudo rights can be managed straight from the LDAP \!

- install sudo-ldap

`apt-get install sudo-ldap`

- Edit the file /etc/sudo-ldap.conf:

`URI    `<ldap://192.168.255.1>  
`BASE`
