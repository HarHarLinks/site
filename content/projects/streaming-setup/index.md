---
title: "Permanent Streaming setup"
linktitle: "Streaming @HSBXL"
date: 2022-09-10
state: running
maintainer: "jurgen"
---

# Context and background

A desktop computer is being set up in the space allowing to turn it on, having all the basics to stream to the social media channels of choice.
This can be used for local events, or for events organized by others in our space.

# Available material

- computer, dual screen (currently only one attached), mouse, keyboard
- webcam (Logitech 720 - from Wouter)
- HDMI grabber
- green screen (needs to be set up - from Jurgen)
- Blue Yeti microphone (ask Jurgen)

# To Do

- [x] device is running Debian
- [x] installed OBS studio
- [ ] install droidcamX to allow Android smartphone as webcam
- [ ] configure stream keys
- [ ] set up OBS Studio to show HSBXL style scenes
