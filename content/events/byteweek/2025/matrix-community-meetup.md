---
startdate: 2025-01-31
starttime: "13:00"
enddate: 2025-01-31
endtime: "21:00"
allday: true
linktitle: "Matrix Foundation & Community Meetup"
title: "Matrix Foundation & Community Meetup"
location: "HSBXL"
eventtype: "Barcamp"
price: "Free"
series: "byteweek2025"
image: "matrix-favicon-white.png"
---

# Matrix Foundation & Community Meetup

There is a large interest in FOSDEM from the Matrix community, and some folks have decided the officially alotted time [for the Matrix track](https://fosdem.org/2025/schedule/track/matrix/) at FOSDEM is not enough and are organizing this additional pre-FOSDEM meetup.

Hackerspace Brussels has offered to supply the location for this event.
There's heating, food, WiFi, and a setup to give presentations, do workshops, and more.
