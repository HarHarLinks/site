---
startdate: 2025-01-28
starttime: "09:00"
enddate: 2025-01-28
endtime: "17:00"
allday: true
linktitle: "FreeCAD hackathon"
title: "FreeCAD hackathon"
location: "HSBXL"
eventtype: "Hackaton"
price: "Free"
series: "byteweek2025"
image: "FreeCAD.svg"
---

# FreeCAD Hackathon

The [FreeCAD project](https://freecad.org) is organizing this year again a hackathon at HSBXL, prior to FOSDEM. We will sit around to code and try to solve some issues together. If you'd like to dive into the FreeCAD source code, that's your opportunity! Come and join us! There will also be a presence on [Discord](https://discord.com/invite/uh85ZRNcfk). More details on the [FreeCAD blog](https://blog.freecad.org/category/events/).
