---
techtuenr: "653"
startdate: 2022-11-01
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue653
series: TechTuesday
title: TechTuesday 653
linktitle: "TechTue 653"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
