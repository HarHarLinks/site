---
linktitle: "TechTuesday"
title: "TechTuesday"
location: "HSBXL"
eventtype: ""
price: "Free"
aliases: [/techtuesday/]
series: "TechTuesday"
start: "true"
---

** Every Tuesday is TechTuesday at HSBXL **. It's where tech enthusiasts hang out, projects get tinkered with, and the occasional debate over the best programming language ensues.

Whether you're an old hand at hacking or just dipping your toes:

- **Chats & Code**: Engage in hushed tech tales, showcase your latest digital masterpiece, or simply bask in the mesmerizing rhythm of coders at work.

- **Ambient Rhythms**: We curate a backdrop of everything from ambient rock to downtempo electronica. It sets the mood, enhancing the creativity without becoming a distraction.

- **Refreshment Protocol**: Club Mate to fuel your creativity, assorted beers for contemplation, and soft drinks for a refreshing pause. Choose your brew.

- **Machinery Magic**: Our space boasts more than just computers. Soldering irons hiss and 3D printers hum, telling tales of projects brought to life.

- **Knowledge Exchange**: Encountered a tech puzzle? Someone in the room might have just the insight you need. And if you’ve unraveled a digital mystery, consider sharing it.

## Upcoming TechTuesdays

{{< events when="upcoming" series="TechTuesday" >}}

## Past TechTuesdays

{{< events when="past" series="TechTuesday" >}}
