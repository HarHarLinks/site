---
techtuenr: "609"
startdate: 2021-12-28
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue609
series: TechTuesday
title: TechTuesday 609
linktitle: "TechTue 609"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
