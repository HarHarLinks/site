---
techtuenr: "597"
startdate: 2021-10-05
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue597
series: TechTuesday
title: TechTuesday 597
linktitle: "TechTue 597"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
