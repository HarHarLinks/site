---
startdate: 2021-10-19
starttime: "18:30"
linktitle: "Hands-on: Tweaking the HSBXL website"
title: "Tweaking the HSBXL website"
price: "Free"
series: "website"
eventtype: "members"
location: "HSBXL"
---

The HSBXL website is based on [Hugo](https://gohugo.io/). Here are a bunch of improvements to be made to the site:

- Improve the system of showing images on a page
- Finishing our own GITLab instance and integrating LDAP
