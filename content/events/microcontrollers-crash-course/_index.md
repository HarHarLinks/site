---
linktitle: "Arduino and microcontrollers"
title: "Arduino and microcontrollers : crash course"
location: "HSBXL"
eventtype: "Workshop"
price: "Free"
aliases: [/arduino_microcontrollers/]
series: "Arduino and microcontrollers"
start: "true"
image: "chip-indigo.png"
---

## Language

This workshop will be given in French and English.

## Workshop

** A deep dive into arduino and microcontrollers development **. We'll follow the guidelines from the "Crash Course Arduino and Microcontroller Development" online training (https://www.udemy.com/course/crash-course-arduino-and-microcontroller-development/).

The course is designed for people who have no prior knowledge of microcontrollers, programming or electronics in general, and so will be the workshops.

## Requirements

- Your own laptop ! NOTE : The course will be OS agnostic.
- During the workshops we'll build increasingly complex prototypes around the arduino. If you don't own one yet and would want to do the builds by yourself and not just watch how it's done, the _[Elegoo Mega R3 Starter Kit](https://www.amazon.com.be/D%C3%A9marrage-dUtilisation-D%C3%A9butants-Professionnels-Compatible/dp/B01JD043XC/ref=sr_1_6?crid=1T0P28M3X8PU4&dib=eyJ2IjoiMSJ9.r3zqZch0DPRFlIh5OxyaKPkttkEmVLiMpF5I0yxDpuQJc9IOO9DoKq7E-dbnGkYAcfFobXQEtc_IQqXKS505xnAKfawnCXP92xS-5sw6i-eRUI7vrQKd163OpdRcCjs0mWOGkxxuRvvwCDjv2Fb99XKTmpc5Qx4wS_O9uvUYnnl-v2zgc0N30R_7-Gc7k50e9n8C1DdwN2oHMcbunmTbq7cposBhDnbMpte_kYeh3qUM4Bm6L1-tx6qevxaTtHICG5X2oC7AhRn_ZlZwz2vQxZCjrpOBUrLMZyImrgz9Hbg.dY1tyT_rKrSrDN7Hj7BXDaIeZcP08F5GH58kibh_Ycw&dib_tag=se&keywords=elegoo+kit&qid=1719823347&sprefix=elegoo+kit%2Caps%2C73&sr=8-6)_ is a great bundle to start with.
