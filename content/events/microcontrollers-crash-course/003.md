---
mccrashcoursenr: "744"
startdate: "2024-07-20"
starttime: "14:30"
endtime: "18:30"
price: "free"
eventtype: "Workshop"
series: "Arduino and microcontrollers"
title: "Arduino and microcontrollers 003"
linktitle: "Arduino and microcontrollers 003"
location: HSBXL
image: "chip-indigo.png"
---

## Language

This workshop will be given in French and English.

## Arduino and microcontroller : Crash Course N°003

**Program :**

- **Replicate our DOOM music machine on simulators** (TinkerCAD & Wokwi)

- **Create a game of Simon with Arduino**
