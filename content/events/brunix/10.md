---
linktitle: BruNix Monthly Meeting September
series: BruNix Monthly Meeting
title: "BruNix Monthly Meeting: Explore Nix with Enthusiasts"
location: HSBXL
eventtype: meetup
startdate: "2024-12-09"
starttime: "18:00"
endtime: "20:00"
image: nix-social-preview.svg
---

## BruNix Monthly Meeting: Explore Nix with Enthusiasts

Join the Brussels Nix User Group for their monthly meeting! This event is perfect for anyone interested in Nix, from beginners to experts. Bring your laptop for an engaging session of learning and networking.

The meeting will feature presentations, discussions, hacking, and networking opportunities. The venue will provide food, so please include any dietary preferences or restrictions in the [Cryptpad document here](https://cryptpad.fr/code/#/2/code/edit/FKByLDQUL34beKWqi6nNnB2c/embed/).

### How to get to HSBXL

For directions to HSBXL, you can find [detailed instructions here](https://hsbxl.be/enter/).

### More Information

For further details and discussions, join the [BruNix Matrix group](https://matrix.to/#/#brunix:hacktheplanet.be). Learn more about the event at [BruNix's website](https://brunix.glitch.me/).

Enjoy a collaborative environment, improve your Nix skills, and connect with fellow enthusiasts in this engaging meetup.
