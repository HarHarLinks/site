---
startdate: 2024-11-24
starttime: "12:00"
endtime: "20:00"
linktitle: "Become anonymous with TAILS and Whonix"
title: "Become anonymous with TAILS and Whonix (FR-EN)"
price: "Free"
image: "tor.jpg"
series: "Security for dummies 3 : revenge harder"
eventtype: "workshop & talks"
location: "HSBXL"
---

## Language

This workshop will be given in French and English.

## Workshop

This workshop aims to be an introduction to online privacy and security using the live-USB linux distribution TAILS (The Amnesiac Incognito Live System) and/or a Whonix virtual machine. We will create bootable TAILS usb keys, setup Whonix on VirtualBox, learn some useful tips about how to protect your anonymity online and share some useful/interesting links accessible through TOR.

## Requirements

- Your trusty laptop
- A usb key with a minimum size of 8Gb (USB 3.0 and 16Gb or higher recommended)

Photo by RealToughCandy.com from Pexels
