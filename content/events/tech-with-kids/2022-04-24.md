---
startdate: 2022-04-24
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids: learn your computer to have fun (AI)"
title: "Tech with kids: learn your computer to have fun (AI)"
price: "Free donation"
image: "tech_with_kids.jpg"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

## !! This workshop is canceled/postponed.

To stay informed on a possible new date for this workshop, place a comment beneath or keep an eye on the website...

---

_In the workshop we are going to laugh, tell jokes and learn how to make our laptops laugh with us. We will do this by recording our gags and giggles with a set of microphones – we will need these recordings to attempt to teach our computer companions a tiny bit of humour. [Hilde Wollenstein](https://web.archive.org/https://www.hilde-wollenstein.com/) and [Andrejs Poikāns](https://web.archive.org/http://www.andrejspoikans.com/), two artists from The Hague are taking us on an artsy trip..._

_This will be done by exploring simple A.I. learning techniques and sound analysis/synthesis algorithms (using [SuperCollider](https://supercollider.github.io/) and [Wekinator](http://www.wekinator.org/) )._

_The purpose of this workshop is to raise questions about what it means to be human in a world full of computers._

## Requirements

Bring a smartphone

## Price

The workshop is free, but we ask for a voluntary donation between 3 and 15 euros, to help cover the travel expenses from The Hague to Brussels and back.

## Language

The workshop will be given primarily in English, but Dutch speaking participants are also welcome, as Hilde also speaks Dutch.

## Registration

Registration to the workshop is required as we only have a limited number of headsets.
Put your name in the comments and tell us with how many you'll come. Add your e-mail in the appropriate field - this won't be visible on the site but allows to confirm your presence.
