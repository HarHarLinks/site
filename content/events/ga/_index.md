---
linktitle: "General Assembly"
title: "General Assembly"
location: "HSBXL"
aliases: [/ga]
series: "GA"
start: "true"
---

As a Belgian non-profit, we need to have at least one general assembly each year.

# Upcoming general assemblies

{{< events when="upcoming" series="GA" >}}

# Past general assemblies

{{< events when="past" series="GA" >}}

# Interesting reads on general assemblies

- http://www.kluwereasyweb.be/documents/voorbeeld-artikels/20140507-algemenevergaderingvzw-assembleegeneraleasbl-hauptversammlungvog.xml

## tldr;

De volgende bevoegdheden zijn voorbehouden aan de algemene vergadering:

- de wijziging van de statuten
- de benoeming en de afzetting van de bestuurders
- de benoeming en de afzetting van de commissarissen en de bepaling van hun bezoldiging ingeval een bezoldiging wordt toegekend
- de kwijting aan de bestuurders en de commissarissen
- de goedkeuring van de begroting en van de rekening
- de ontbinding van de vereniging
- de uitsluiting van een lid
- de omzetting van de vereniging in een vennootschap met sociaal oogmerk
- alle gevallen waarin de statuten dat vereisen

Alles wat in de wet of de statuten niet is toegekend aan de algemene vergadering, valt onder de
bevoegdheid van de raad van bestuur.
