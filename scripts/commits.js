import fs from "node:fs/promises";
import path from "node:path";
import crypto from "node:crypto";
import { simpleGit } from "simple-git";
import { glob } from "glob";
import pMap from "p-map";

console.log("Generating git logs of each *.md file for changelogs.");

const git = simpleGit();

const generateFileName = (filePath) =>
  crypto.createHash("md5").update(filePath).digest("hex");

const getGitLogForFile = async (filePath) => {
  return (
    await git.log({
      file: filePath,
      format: {
        commit: "%H",
        author: "%aN",
        commit_date: "%ci",
        message: "%s",
      },
    })
  ).all;
};

const contentDir = process.argv[2];
const outputDir = process.argv[3];

const checkForConflicts = async (dir) => {
  const directories = await glob(`${dir}/**/`);
  for (const subdir of directories) {
    const indexFilePath = path.join(subdir, "_index.md");
    const baseFilePath = path.join(dir, path.relative(dir, subdir) + ".md");

    if (
      (await fs
        .access(indexFilePath)
        .then(() => true)
        .catch(() => false)) &&
      (await fs
        .access(baseFilePath)
        .then(() => true)
        .catch(() => false))
    ) {
      console.error(
        `Conflict detected: Both ${indexFilePath} and ${baseFilePath} exist.`,
      );
      process.exit(1);
    }
  }
};

await checkForConflicts(contentDir);

const files = await glob(`${contentDir}/**/*.md`);

await pMap(
  files,
  async (filePath) => {
    const relativePath = path.relative(contentDir, filePath);
    const fileName = generateFileName(relativePath);
    const log = await getGitLogForFile(filePath);
    const outputFilePath = path.join(outputDir, `${fileName}.json`);
    await fs.writeFile(outputFilePath, JSON.stringify(log, null, 2));
  },
  { concurrency: 10 },
);

console.log("Generated logs for " + files.length + " file(s).");
