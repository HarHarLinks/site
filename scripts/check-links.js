import { LinkChecker } from "linkinator";
import handler from "serve-handler";
import http from "node:http";

const CHECK_REMOTE = process.argv[2] === "remote";

if (CHECK_REMOTE) {
  console.log("Checking remote URLs");
} else {
  console.log("Checking local URLs");
}

const server = http.createServer((req, res) => {
  handler(req, res, {
    public: "./public",
  });
});

const port = await new Promise((resolve) => {
  server.listen(0, () => {
    resolve(server.address().port);
  });
});

const LOCAL_URL = "http://localhost:" + port;
const hideLocalhostURL = (url) => url?.replace(LOCAL_URL, "");

const checker = new LinkChecker();

checker.on("link", (link) => {
  if (link.state !== "SKIPPED") {
    console.log("Checking ", hideLocalhostURL(link.url));
  }
});

const res = await checker.check({
  path: LOCAL_URL,
  recurse: true,
  timeout: 5000,
  concurrency: CHECK_REMOTE ? 3 : 10,
  retry: true,
  retryErrors: false,
  linksToSkip: async (link) => {
    if (link.startsWith("https://gitlab.com/hsbxl")) {
      return true;
    }

    if (link.startsWith("https://www.brussels.be")) {
      // they are missing TLS certificates, seems to load corectly in Chrome
      return true;
    }

    const url = new URL(link);
    if (url.protocol !== "http:" && url.protocol !== "https:") {
      return true;
    }

    if (!CHECK_REMOTE && url.origin !== LOCAL_URL) {
      return true;
    }

    return false;
  },
});

const broken = res.links
  .filter((link) => {
    if (!CHECK_REMOTE) {
      return true;
    }
    const url = new URL(link.url);
    if (url.origin === LOCAL_URL) {
      return false;
    }
    return true;
  })

  .filter((link) => link.state === "BROKEN" && link.status !== 403);
if (broken.length) {
  broken.forEach((link) => {
    console.error(
      link.status || "",
      "ERROR Broken link: " +
        hideLocalhostURL(link.url) +
        " on page " +
        hideLocalhostURL(link.parent),
    );
    console.warn(
      hideLocalhostURL(
        (link.failureDetails.at(-2) ?? link.failureDetails.at(-1)).toString(),
      ),
    );
  });
  process.exit(1);
} else {
  console.log("All links are good!");
  process.exit(0);
}
